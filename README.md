# The base module of the [tex2x-converter](https://gitlab.tubit.tu-berlin.de/stefan.born/VEUNDMINT_TUB_Brueckenkurs)

This module is used to showcase the capabilities of the [tex2x-converter](https://gitlab.tubit.tu-berlin.de/stefan.born/VEUNDMINT_TUB_Brueckenkurs). Its goal
is to offer developers / content editors as much guidance as possible in creating their own modules. It is also used for running the tests of the tex2x-converter.

## Getting Started

Refer to the Getting started guide in [tex2x-converter](https://gitlab.tubit.tu-berlin.de/stefan.born/VEUNDMINT_TUB_Brueckenkurs)

## Authors

* **Daniel Haase** - *Initial work*
* **Niklas Jurij Plessing** - Adaptations and improvements
* **Alvaro Ortiz**
* **Bjoern Jeschke**
